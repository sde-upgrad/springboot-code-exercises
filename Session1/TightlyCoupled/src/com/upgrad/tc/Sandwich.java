package com.upgrad.tc;

public class Sandwich {

    Bread bread;

    public Bread getBread() {
        return bread;
    }

    public void setBread(Bread bread) {
        this.bread = bread;
    }
}
