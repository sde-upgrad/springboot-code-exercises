package com.upgrad.tc;

public class Bread {

    String type;

    public Bread() {
    }

    public Bread(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
