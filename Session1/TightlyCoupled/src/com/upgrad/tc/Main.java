package com.upgrad.tc;

public class Main {

    public static void main(String[] args) {
	 Sandwich sw =  new Sandwich();
        /**
         * Tight coupling
         * If Bread changes then this will require changes in code
         * Problem of tight coupling is due to the use of new
         */
	 sw.setBread(new Bread());
	 sw.setBread(new Bread("TYPE1"));
    }
}
