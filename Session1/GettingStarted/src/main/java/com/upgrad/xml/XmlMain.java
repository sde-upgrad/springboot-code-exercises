package com.upgrad.xml;

import com.upgrad.model.Sandwich;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class XmlMain {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("app.xml");
        Sandwich sw =  (Sandwich)context.getBean("sandwich");
        sw.makeSandwich();
    }

}
