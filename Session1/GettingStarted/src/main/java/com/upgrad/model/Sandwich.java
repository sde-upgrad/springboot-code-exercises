package com.upgrad.model;

public class Sandwich {
    Bread bread;
    Cheese cheese;
    Vegetables vegetables;

    public void makeSandwich(){
        bread.addBread();
        cheese.addCheese();
        vegetables.addVegetables();
    }

    public Bread getBread() {
        return bread;
    }

    public void setBread(Bread bread) {
        this.bread = bread;
    }

    public Cheese getCheese() {
        return cheese;
    }

    public void setCheese(Cheese cheese) {
        this.cheese = cheese;
    }

    public Vegetables getVegetables() {
        return vegetables;
    }

    public void setVegetables(Vegetables vegetables) {
        this.vegetables = vegetables;
    }
}
