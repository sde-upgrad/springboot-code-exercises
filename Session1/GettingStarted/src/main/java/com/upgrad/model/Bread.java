package com.upgrad.model;

public class Bread {

    Flour flour;
    BakingPowder bakingPowder;
    Flavors flavors;

    public Flour getFlour() {
        return flour;
    }

    public void setFlour(Flour flour) {
        this.flour = flour;
    }

    public BakingPowder getBakingPowder() {
        return bakingPowder;
    }

    public void setBakingPowder(BakingPowder bakingPowder) {
        this.bakingPowder = bakingPowder;
    }

    public Flavors getFlavors() {
        return flavors;
    }

    public void setFlavors(Flavors flavors) {
        this.flavors = flavors;
    }

    public void addBread(){
        flour.addFlour();
        bakingPowder.addBakingPowder();
        flavors.addFlavors();
        System.out.println("Adding Bread");
    }
}
