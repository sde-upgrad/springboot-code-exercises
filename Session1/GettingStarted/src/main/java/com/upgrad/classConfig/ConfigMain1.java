package com.upgrad.classConfig;

import com.upgrad.model.Sandwich;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ConfigMain1 {

    public static  void main(String args[]){
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigEg.class);
        Sandwich sw =  (Sandwich)ctx.getBean("sandwich");
        sw.makeSandwich();
    }
}
