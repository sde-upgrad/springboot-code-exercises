package com.upgrad.classConfig;

import com.upgrad.model.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigEg {

    @Bean
    public Cheese cheese(){
        return new Cheese();
    }

    @Bean
    public Vegetables vegetables(){
        return new Vegetables();
    }

    @Bean
    public Flour flour(){
        return new Flour();
    }

    @Bean
    public BakingPowder bakingPowder(){
        return new BakingPowder();
    }

    @Bean
    public Flavors flavors(){
        return  new Flavors();
    }

    @Bean
    public Bread bread(){
        Bread bread = new Bread();
        bread.setBakingPowder(bakingPowder());
        bread.setFlavors(flavors());
        bread.setFlour(flour());
        return  bread;
    }

    @Bean
    public Sandwich sandwich(){
        Sandwich sw = new Sandwich();
        sw.setBread(bread());
        sw.setCheese(cheese());
        sw.setVegetables(vegetables());
        return sw;
    }
}
