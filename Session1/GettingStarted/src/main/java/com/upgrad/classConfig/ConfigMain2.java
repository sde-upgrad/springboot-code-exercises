package com.upgrad.classConfig;

import com.upgrad.model.Sandwich;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ConfigMain2 {

    public static  void main(String args[]){
        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(ConfigEg.class);
        ctx.refresh();
        Sandwich sw =  (Sandwich)ctx.getBean("sandwich");
        sw.makeSandwich();
    }
}
