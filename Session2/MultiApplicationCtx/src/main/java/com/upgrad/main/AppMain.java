
package com.upgrad.main;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AppMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("setterBean.xml");
		Emp emp =(Emp)context.getBean("empBean");
		System.out.println(emp.getEmpName());
		Dept dept = emp.getDept();
		System.out.println(dept.getDeptName());


		ClassPathXmlApplicationContext context1 = new ClassPathXmlApplicationContext("setterBean2.xml");
		Emp emp1 =(Emp)context1.getBean("empBean");
		System.out.println(emp1.getEmpName());
		Dept dept1 = emp.getDept();
		System.out.println(dept1.getDeptName());

	}

}
