package com.upgrad.model;

public class Emp {
	String empName;
	int empId;
	Dept dept;

	public Emp() {
	}

	public Emp(String empName, int empId, Dept dept) {
		this.empName = empName;
		this.empId = empId;
		this.dept = dept;
	}

	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public Dept getDept() {
		return dept;
	}
	public void setDept(Dept dept) {
		this.dept = dept;
	}
	
	

}
