package com.upgrad.xml.constructorEg;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;



public class ConstructorInjectionMain {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("constructorBeans.xml");
		Emp emp = (Emp) context.getBean("empBean");
		System.out.println(emp.getEmpName());
		Dept dept = emp.getDept();
		System.out.println(dept.getDeptName());


	}

}
