package com.upgrad.classConfig.setterEg;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.annotation.Bean;

public class ConfigEg1 {

    @Bean
    public Dept dept(){
        Dept dept =  new Dept();
        dept.setDeptId(1);
        dept.setDeptName("COMP");
        return dept;
    }

    @Bean(name = "empBean")
    public Emp emp(){
        Emp emp =  new Emp();
        emp.setEmpId(1);
        emp.setEmpName("Rama");
        emp.setDept(dept());
        return emp;
    }
}
