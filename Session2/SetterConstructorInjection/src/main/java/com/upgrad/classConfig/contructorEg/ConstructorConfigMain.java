package com.upgrad.classConfig.contructorEg;

import com.upgrad.classConfig.setterEg.ConfigEg1;
import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ConstructorConfigMain {

    public static  void main(String args[]) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigEg2.class);
        Emp emp =(Emp)ctx.getBean("empBean");
        System.out.println(emp.getEmpName());
        Dept dept = emp.getDept();
        System.out.println(dept.getDeptName());
    }
}
