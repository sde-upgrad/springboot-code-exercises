package com.upgrad.classConfig.setterEg;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class SetterConfigMain {

    public static  void main(String args[]) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(ConfigEg1.class);
        Emp emp =(Emp)ctx.getBean("empBean");
        System.out.println(emp.getEmpName());
        Dept dept = emp.getDept();
        System.out.println(dept.getDeptName());
    }
}
