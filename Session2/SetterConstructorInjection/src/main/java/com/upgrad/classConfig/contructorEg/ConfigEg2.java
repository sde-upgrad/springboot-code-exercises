package com.upgrad.classConfig.contructorEg;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.annotation.Bean;

public class ConfigEg2 {

    @Bean
    public Dept dept(){
        Dept dept =  new Dept();
        dept.setDeptId(1);
        dept.setDeptName("COMP");
        return dept;
    }

    @Bean(name = "empBean")
    public Emp emp(){
        Emp emp =  new Emp("Rama",1,dept());
        return emp;
    }
}
