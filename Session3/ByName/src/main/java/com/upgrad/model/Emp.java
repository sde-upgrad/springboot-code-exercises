package com.upgrad.model;

import org.springframework.beans.factory.annotation.Autowired;

public class Emp {

    int empId;
    String empName;
    @Autowired
    Dept dept;

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public Dept getDept() {
        return dept;
    }

    public void setDept(Dept dept) {
        this.dept = dept;
    }
}
