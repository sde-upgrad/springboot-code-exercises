package com.upgrad;

import com.upgrad.config.ConfigEg;
import com.upgrad.model.Emp;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ByTypeMain {

    public static  void  main(String args[]){
        AnnotationConfigApplicationContext ctx =  new AnnotationConfigApplicationContext(ConfigEg.class);
        Emp e =   ctx.getBean("emp",Emp.class);
        System.out.println("Dept of emp -->"+e.getDept().getDeptId()+","+e.getDept().getDeptName());
    }
}
