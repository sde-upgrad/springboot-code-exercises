package com.upgrad.config;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigEg {

    @Bean
    public Emp emp(){
        Emp e =  new Emp();
        e.setEmpName("Rama");
        e.setEmpId(1);
        return e;
    }

    @Bean
    public Dept dept2(){
        Dept d = new Dept();
        d.setDeptName("COMP");
        d.setDeptId(1);
        return d;
    }

   /* @Bean
    public Dept dept1(){
        Dept d = new Dept();
        d.setDeptName("IT");
        d.setDeptId(2);
        return d;
    }*/
}
