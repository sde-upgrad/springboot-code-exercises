package com.upgrad;

import com.upgrad.model.Emp;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ByConstructorXmlMain {

    public static  void  main(String args[]){
        ClassPathXmlApplicationContext ctx =  new ClassPathXmlApplicationContext("byConstructor.xml");
        Emp e =   ctx.getBean("emp", Emp.class);
        System.out.println("Dept of emp -->"+e.getDept().getDeptId()+","+e.getDept().getDeptName());
    }
}
