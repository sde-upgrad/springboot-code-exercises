package com.upgrad.config;

import com.upgrad.model.Dept;
import com.upgrad.model.Emp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.upgrad.model")
public class ConfigEg {



    @Bean
    public Dept dept2(){
        Dept d = new Dept();
        d.setDeptName("COMP");
        d.setDeptId(1);
        return d;
    }


}
